# Coursework 2 - Web Services



## Right Job Structure


### Right Job Parser service is located in the go-pdfparser folder
This service can be found here https://right-job-parser2.herokuapp.com/

### Right Job Info service is located in the python-job-info folder
This service can be found here https://right-job.herokuapp.com/

### Right Job Client is located in the right-job-client folder
Our integrated client can be found here https://right-job-client.herokuapp.com/



