package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strings"

	"github.com/dslipak/pdf"
)

//Get text part of pdf
func readPdf(path string) (string, error) {
	r, err := pdf.Open(path)
	// remember close file
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	b, err := r.GetPlainText()
	if err != nil {
		return "", err
	}
	buf.ReadFrom(b)
	return buf.String(), nil
}

func isNumber(s string) bool {
	for _, c := range s {
		if c < '0' || c > '9' {
			return false
		}
	}
	return true
}

func isAlphanum(s string) bool {
	for _, c := range s {
		if (c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') {
			return false
		}
	}
	return true
}

//change delimiters to spaces
func cleanDelims(s string) string {
	const delims string = ",.!?:'\"-"
	res := ""
	for i := 0; i < len(s); i++ {
		found := false
		for _, delim := range delims {
			if rune(s[i]) == delim {
				found = true
				break
			}
		}
		if found {
			res += " "
		} else {
			res += string(s[i])
		}
	}
	return res
}

// Find most frequent keywords in string
func ListKeywordsContent(pdfcontent string) []string {
	words := strings.Split(cleanDelims(pdfcontent), " ")
	var file *os.File
	stopcontent, err := ioutil.ReadFile("stop_words_english.txt")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	stopwords := string(stopcontent)
	file.Close()

	//remove stopwords and count word occurences
	ranking := make(map[string]int)
	for i := range words {
		words[i] = strings.ToLower(strings.TrimSpace(words[i]))
		if !(strings.Contains(stopwords, words[i]) || isNumber(words[i])) && isAlphanum(words[i]) {
			ranking[words[i]]++
		}
	}

	//find most common words
	topcount := int(math.Min(5.0, float64(len(ranking))))
	topwords := make([]string, topcount)
	for i := 0; i < topcount; i++ {
		maxcount := 0
		for key, count := range ranking {
			if count > maxcount {
				maxcount = count
				topwords[i] = key
			}
		}
		ranking[topwords[i]] = 0
		fmt.Printf("%s %d\n", topwords[i], maxcount)
	}
	return topwords
}

// Read contents of file fname and find most frequent keywords
func ListKeywordsFile(fname string) []string {
	pdfcontent, err := readPdf(fname)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return ListKeywordsContent(pdfcontent)
}
