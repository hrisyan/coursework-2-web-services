# Execution

This service requires **Go 1.17** to be installed.

To run this service, type:
    
    go get .
    go run .
