package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type ServerResponse struct {
	Message string `json:"message"`
}

// save pdf locally and generate keywords
func uploadPdf(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.String(http.StatusBadRequest, "Error receiving file")
		return
	}

	//resolve filename
	fname := c.PostForm("name")
	if fname == "" {
		fname = filepath.Base(file.Filename)
	}

	c.SaveUploadedFile(file, ("resources/" + fname))
	keywords := ListKeywordsFile("resources/" + fname)
	keyfile, _ := os.Create("resources/" + fname + ".key")

	for _, s := range keywords {
		keyfile.WriteString(s + " ")
	}

	keyfile.Close()
	c.IndentedJSON(http.StatusCreated, keywords)
}

// list saved files with generated keywords
func listFiles(c *gin.Context) {
	dirfiles, err := ioutil.ReadDir("resources/")
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, "Internal Error: Failed to read resource list")
		fmt.Println(c.ClientIP() + ":" + err.Error())
		return
	}
	filenames := make([]string, len(dirfiles))

	for i := range dirfiles {
		filenames[i] = dirfiles[i].Name()
	}
	c.IndentedJSON(http.StatusOK, filenames)
}

//send generated keywords
func getKeywordsFilename(c *gin.Context) {
	fname := c.Param("name")
	filebytes, err := ioutil.ReadFile("resources/" + fname + ".key")
	if err != nil {
		c.IndentedJSON(http.StatusNotFound, ServerResponse{"File not found"})
		fmt.Printf("%s", err.Error())
		return
	}
	stringarr := strings.Split(string(filebytes), " ")
	if stringarr[len(stringarr)-1] == "" {
		//trim trailing empty string
		stringarr = stringarr[0 : len(stringarr)-1]
	}
	c.IndentedJSON(http.StatusOK, stringarr)
}

func main() {
	//basic server settings
	port, found := os.LookupEnv("PORT")
	if !found {
		port = "8080" //default port if none is specified
	}
	router := gin.Default()

	//setup cors for client
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true

	//setup requests
	router.Use(cors.New(corsConfig))
	router.GET("/listall", listFiles)
	router.GET("/getkeywords/:name", getKeywordsFilename)
	router.POST("/upload", uploadPdf)

	//execute
	router.Run("0.0.0.0:" + port)
}
