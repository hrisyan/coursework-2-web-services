from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
cors = CORS(app)
api = Api(app)
app.config['CORS_HEADERS'] = 'Content-Type'
# Update the like value in the jobs array
def likeIt(data,like,c):
    for i in data["jobs"]:
        if(int(i["id"])==int(like)):
            val=1
            try:
                if(int(i["like"])==1 and c==0):                
                    val=0       
                
            except KeyError:
                if(c==1):
                    val=1
            #print(val)
            i.update({"like":str(val)})
            with open('data.txt', 'w') as outfile:
                json.dump(data, outfile)

class jobs(Resource):
    # Handle get requests
    def get(self):
        #If option parametar is all it returns all jobs
        option = request.args.get('option')
        #Gets keywords with a ',' as dilimeter
        search = request.args.get('search')
        #If fav has a value the favourite list is returned
        fav = request.args.get('fav')
        #Read the saved data
        with open('data.txt', 'r') as outfile:
            data=json.load(outfile)
        if(option=="all"):           
            print(data["jobs"])
            return {'jobs': data["jobs"]}
        if(fav!=None):
            result=[]
            for i in data["jobs"]:
                try:
                    if(int(i["like"])==1):
                        result.append(i)
                except KeyError:
                    continue
            return {'job-count':len(result),'jobs': result}
        if(search!=None):       
            result=[]
            allWords = str(search).split(",")
            # the search checks the wheter the string is in the discription or the title of the job
            for i in data["jobs"]:
                for word in allWords:
                    # we lower both strins so we can compare them regardless of capital letters 
                    if(str(i["title"]).lower().find(word.lower())!=-1):
                        result.append(i)
                        # We break so that all the jobs are unique
                        break
                    # we lower both strins so we can compare them regardless of capital letters 
                    if(str(i["description"]).lower().find(word.lower())!=-1):
                        result.append(i)
                        # We break so that all the jobs are unique
                        break

            return {'job-count':len(result),'jobs': result}
    # Handle post requests
    def post(self):
        #Both rm and like should be a job id 
        like = request.args.get('like')
        rm = request.args.get('rm')
        save = request.args.get('save')
        with open('data.txt', 'r') as outfile:
            data=json.load(outfile)
    
        # if the id of the job is correct it adds it to the favourites list or if it is already added it removes it.
        if(like!=None):
            likeIt(data,like,0)
            return {'status': "OK"}
        #if the id of the job is correct it is removed from the jobs list.
        if(rm!=None):
            for i in data["jobs"]:
                if(int(i["id"])==int(rm)):
                    data["jobs"].remove(i)
            with open('data.txt', 'w') as outfile:
                json.dump(data, outfile)
            return {'status': "OK"}


        #it gets a JSON array of jobs as input and replaces the old job list. If some of the jobs from the favourite list exist in the new job list they remain in the favourite list.
           
        if(save!=None):
            data_ = request.get_json()
            for i in data["jobs"]:
                
                try:
                    if(int(i["like"])==1):
                        likeIt(data_,i["id"],1)
                except KeyError:
                    continue
                 
            with open('data.txt', 'w') as outfile:
                json.dump(data_, outfile)
            return {'status': "OK"}





api.add_resource(jobs, '/jobs')



if __name__ == '__main__':
    app.run(debug=True)
