var allJobs;



const loadJobs = async(option) => {
    let headers = new Headers();


    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');



    headers.append('GET', 'POST', 'OPTIONS');


    const response = await fetch('https://right-job.herokuapp.com/jobs?' + option);
    allJobs = await response.json();
    console.log(allJobs['jobs'][0])
    var itm = document.getElementById("t1");
    itm.setAttribute("data-index", 0);
    el = itm.getElementsByTagName('td')
    el[0].innerHTML = 1
    el[1].getElementsByTagName('button')[0].setAttribute("onclick", "window.location.href='" + allJobs['jobs'][0]["url"] + "'")
    el[2].innerHTML = allJobs['jobs'][0]["title"]
    el[3].innerHTML = allJobs['jobs'][0]["company_name"]

    el[4].getElementsByClassName('btn-primary')[0].setAttribute("href", "#c" + 0)
    el[4].getElementsByTagName("div")[0].setAttribute("id", "c" + 0)
    el[4].getElementsByClassName('card-body')[0].innerHTML = allJobs['jobs'][0]["description"]
    if (allJobs['jobs'][0]["like"] == 1) {
        el[5].childNodes[0].childNodes[1].setAttribute('fill-opacity', '1')
    }
    // Copy the <li> element and its child nodes
    for (var i = 1; i < allJobs['jobs'].length; i++) {

        //allJobs['jobs'][0]
        var cln = itm.cloneNode(true)

        cln.setAttribute("data-index", i);
        el = cln.getElementsByTagName('td')
        el[0].innerHTML = i + 1
        el[1].getElementsByTagName('button')[0].setAttribute("onclick", "window.location.href='" + allJobs['jobs'][i]["url"] + "'")
        el[2].innerHTML = allJobs['jobs'][i]["title"]
        el[3].innerHTML = allJobs['jobs'][i]["company_name"]

        el[4].getElementsByClassName('btn-primary')[0].setAttribute("href", "#c" + i)
        el[4].getElementsByTagName("div")[0].setAttribute("id", "c" + i)
        el[4].getElementsByClassName('card-body')[0].innerHTML = allJobs['jobs'][i]["description"]
        el[5].childNodes[0].childNodes[1].setAttribute('fill-opacity', '0')
        if (allJobs['jobs'][i]["like"] == 1) {
            console.log(allJobs['jobs'][i])
            el[5].childNodes[0].childNodes[1].setAttribute('fill-opacity', '1')
        }
        // Append the cloned <li> element to <ul> with id="myList1"
        document.getElementById("parent").appendChild(cln)
    }
}


function likeIt(el, myel) {
    console.log("L");
    oldF = myel.childNodes[1].getAttribute('fill-opacity')
    var i = el.getAttribute('data-index');
    likeJobs(allJobs['jobs'][i]['id'])
    if (oldF == 0) {
        myel.childNodes[1].setAttribute('fill-opacity', '1')


    } else {
        myel.childNodes[1].setAttribute('fill-opacity', '0')
    }


}
const likeJobs = async(num) => {
    let headers = new Headers();


    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('GET', 'POST', 'OPTIONS');


    const response = await fetch('https://right-job.herokuapp.com/jobs?like=' + num, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const res = await response.json();
    console.log(res)

}


function removeIt(el, myel) {

    var i = el.getAttribute('data-index');
    removeJobs(allJobs['jobs'][i]['id'])
    p = document.getElementById("parent");
    p.removeChild(p.childNodes[i]);

}
const removeJobs = async(num) => {
    let headers = new Headers();


    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('GET', 'POST', 'OPTIONS');


    const response = await fetch('https://right-job.herokuapp.com/jobs?rm=' + num, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const res = await response.json();
    console.log(res)

}


const updateJobs = async() => {

    const response = await fetch('https://remotive.io/api/remote-jobs?category=software-dev');
    jobs = await response.json();
    console.log(jobs)
    saveJobs(jobs)
}

const saveJobs = async(data) => {
    // let headers = new Headers();


    // headers.append('Content-Type', 'application/json');
    // headers.append('Accept', 'application/json');
    // headers.append('GET', 'POST', 'OPTIONS');


    // const response = await fetch('https://right-job.herokuapp.com/jobs?save=1', {
    //     method: 'POST',
    //     body: data,
    //     headers: headers
    // });
    // const res = await response;
    // console.log(res)
    var url = "https://right-job.herokuapp.com/jobs?save=1";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            alert("Update Finished");
            location.reload();
        }
    };

    // var data = '{"jobs":"d"}';

    xhr.send(JSON.stringify(data));

}