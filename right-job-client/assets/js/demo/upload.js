function keywords(word) {
    a = document.createElement("a")
    div = document.createElement("a")
    itm = document.getElementById("cross")
    cln = itm.cloneNode(true)
    cln.setAttribute("style", "")
    div.setAttribute("class", "divkey")
    a.innerHTML = word
    a.setAttribute("class", "key")
    div.appendChild(a)
    div.appendChild(cln)
    document.getElementById("kparent").appendChild(div)


}


function rmWord(itm, par) {
    par.removeChild(itm);
}

function addKeywords() {
    fname = document.getElementById("addKeywords").keyword.value;
    document.getElementById("keyword").value = "";
    keywords(fname);
}
var Keywords;

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function searchKeywords() {
    arr = document.getElementById("kparent").childNodes;
    res = ""
    for (var i = 1; i < arr.length; i++) {
        res += arr[i].childNodes[0].innerHTML;
        if (i != arr.length - 1) {
            res += ","
        }
    }

    setCookie("keywords", res, 1);



}


function searchAll() {
    setCookie("keywords", "", 1);

}


const getHistory = async() => {


    const response = await fetch('https://right-job-parser2.herokuapp.com/listall');
    const res = await response.json();

    for (var i = 0; i < res.length; i += 2) {
        addHistory(res[i]);
    }


}


const getKeywordsHistory = async(name) => {


    const response = await fetch('https://right-job-parser2.herokuapp.com/getkeywords/' + name);
    const res = await response.json();

    for (var i = 0; i < res.length; i++) {
        keywords(res[i]);
    }

}

function addHistory(filename) {
    itm = document.createElement("a");
    itm.innerHTML = filename;
    itm.setAttribute("id", "temp1");
    itm.setAttribute("onclick", "printKeywordsHistory(this)")
    spacer = document.createElement("p")
    spacer.setAttribute("id", "spacer");
    par = document.getElementById("heading1");
    par.appendChild(spacer);
    par.appendChild(itm);


}

function printKeywordsHistory(el) {
    getKeywordsHistory(el.innerHTML);
}